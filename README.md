# TODO

- [x] basic setup
- [x] multi touch
- ability to change velocity
- ability to change key range
- controlchange nob interface
- proof of concept is done. so let's setup modern frontend architecture.
  - better save/restore architecture
- ? rewrite osc2midi and incorporate the functionality as http2midi
- ? websocket based feedback state from server to client


# Setup

```
[ Start osc2midi ]
$ osc2midi -p 57120 -m osc2midi_mapping.omm -v

[ Start http2osc ]
$ python http2osc.py -a 0.0.0.0 -p 5000 -o osc.udp://localhost:57120

[ UI ]
# open http://osc.hiogawa.net in browser
```


# Dependency

- python
- python liblo
- python werkzeug
- web browser


# References

- https://w3c.github.io/uievents/#event-type-mouseover
- https://www.w3.org/TR/touch-events/
