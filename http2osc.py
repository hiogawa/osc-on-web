import os
import liblo
from werkzeug.wrappers import Request, Response


class App(object):
    def __init__(self, osc_address):
        self._osc_address = osc_address

    def __call__(self, environ, start_response):
        request = Request(environ)
        response = self.dispatch_request(request)
        response.headers['Access-Control-Allow-Origin'] = '*'
        return response(environ, start_response)

    def send_osc(self, params):
        address = params.get("address", self._osc_address)
        path = params.get("path", "/")
        arguments = []

        if params.get("arguments[]"):
            for arg in params.getlist("arguments[]"):
                arguments.append(tuple(arg.split("|")))

        address = liblo.Address(address)
        message = liblo.Message(path, *arguments)
        liblo.send(address, message)

    def dispatch_request(self, request):
        if request.path == "/":
            try:
                self.send_osc(request.args)
                return Response(status=200)
            except Exception as err:
                print(err)
                return Response(str(err), status=400)
        else:
            return Response(status=404)


def main():
    import optparse
    parser = optparse.OptionParser()
    parser.add_option('-a', '--address', dest='address', default='127.0.0.1',
                        help='default value is 127.0.0.1')
    parser.add_option('-p', '--port', dest='port', default=5000, type='int',
                        help='default value is 5002')
    parser.add_option('-o', '--osc-address', dest='osc_address',
                        default="osc.udp://localhost:57120",
                        help='default value is osc.udp://localhost:57120')
    opts = parser.parse_args()[0]

    from werkzeug.serving import run_simple
    from werkzeug.wsgi import SharedDataMiddleware
    app = App(opts.osc_address)
    app = SharedDataMiddleware(app,
        { "/web": os.path.join(os.path.dirname(__file__), 'web') },
        cache=False)
    run_simple(opts.address, opts.port, app, use_reloader=True)


if __name__ == "__main__":
    main()
