//// http request ////
function request (path, args) {
  const http2osc_address = document.querySelector('#http2osc_address').value;
  const osc2midi_address = document.querySelector('#osc2midi_address').value;

  let params = [
      ['address', osc2midi_address],
      ['path', path]
    ]
  params = params.concat(args.map((arg) => ['arguments[]', arg[0] + '|' + arg[1]]));

  const q = params.map((param) => param.join('=')).join('&');

  fetch(encodeURI(http2osc_address + '?' + q));
}

function initializeSettings () {
  const input0 = document.querySelector('#http2osc_address');
  const input1 = document.querySelector('#osc2midi_address');
  const input2 = document.querySelector('#ui_mode');
  input0.value = selected.http2osc_address;
  input1.value = selected.osc2midi_address;
  input2.value = 'touch';
}


/// OSC event ///
const channel = 0;

function noteon (note, velocity) {
  request('/noteon', [['i', channel], ['i', note], ['i', velocity]]);
}

function noteoff (note, velocity) {
  request('/noteoff', [['i', channel], ['i', note], ['i', velocity]]);
}


//// UI event handle ////

const velocity = 100;

function activateKey (node) {
  const note = node.getAttribute('data-note');
  node.classList.add('key--pressed');
  noteon(note, velocity);
}

function deactivateKey (node) {
  const note = node.getAttribute('data-note');
  node.classList.remove('key--pressed');
  noteoff(note, velocity);
}

function touchMode () {
  return document.querySelector('#ui_mode').value === 'touch';
}

function onPress (e) { // mousedown, mouseover
  if (touchMode()) { return; }

  if (e.buttons !== 0) { // skip if no button is pressed
    const node = e.target;
    activateKey(node);
  }

  // Note that black key lives inside of white key in dom topology
  e.stopPropagation();
}

function onRelease (e) { // mouseup, mouseout
  if (touchMode()) { return; }

  const node = e.target;
  if (node.classList.contains('key--pressed')) { // skip if key is not activated
    deactivateKey(node);
  }

  e.stopPropagation();
}

function touchToKey (touch) {
  const blackKeys = Array.from(document.querySelectorAll('.key-black'));
  const whiteKeys = Array.from(document.querySelectorAll('.key-white'));

  for (let i in blackKeys) {
    const blackKey = blackKeys[i];
    const rect = blackKey.getClientRects()[0];
    if (
      rect.top < touch.clientY && touch.clientY < rect.top + rect.height &&
      rect.left < touch.clientX && touch.clientX < rect.left + rect.width
    ) {
      return blackKey;
    }
  };

  for (let i in whiteKeys) {
    const whiteKey = whiteKeys[i];
    const rightNeighberBlackKey = whiteKey.children[0];
    const leftNeighberBlackKey = whiteKey.previousElementSibling && whiteKey.previousElementSibling.children[0];
    const rect = whiteKey.getClientRects()[0];
    const rectR = rightNeighberBlackKey && rightNeighberBlackKey.getClientRects()[0];
    const rectL = leftNeighberBlackKey && leftNeighberBlackKey.getClientRects()[0];
    if (
      rect.top < touch.clientY && touch.clientY < rect.top + rect.height &&
      rect.left < touch.clientX && touch.clientX < rect.left + rect.width &&
      !(rectR &&
        rectR.top < touch.clientY && touch.clientY < rectR.top + rectR.height &&
        rectR.left < touch.clientX && touch.clientX < rectR.left + rectR.width) &&
      !(rectL &&
        rectL.top < touch.clientY && touch.clientY < rectL.top + rectL.height &&
        rectL.left < touch.clientX && touch.clientX < rectL.left + rectL.width)
    ) {
      return whiteKey;
    }
  };
}

function onTouchstart (e) {
  if (!touchMode()) { return; }

  for (let i = 0; i < e.changedTouches.length; i++) {
    const touch = e.changedTouches.item(i);
    const key = touchToKey(touch);
    if (!key.classList.contains('key--pressed')) {
      activateKey(key);
    }
  }
}

function onTouchend (e) {
  if (!touchMode()) { return; }

  for (let i = 0; i < e.changedTouches.length; i++) {
    const touch = e.changedTouches.item(i);
    const key = touchToKey(touch);
    if (key.classList.contains('key--pressed')) {
      deactivateKey(key);
    }
  }
}

function onTouchmove (e) {
  if (!touchMode()) { return; }
  e.preventDefault();

  const keys = Array.from(document.querySelectorAll('.key'));
  const keysToActivate = [];

  for (let i = 0; i < e.touches.length; i++) {
    const touch = e.touches.item(i);
    keysToActivate.push(touchToKey(touch));
  }

  keys.forEach((key) => {
    if (keysToActivate.includes(key)) {
      if (!key.classList.contains('key--pressed')) {
        activateKey(key);
      }
    } else {
      if (key.classList.contains('key--pressed')) {
        deactivateKey(key);
      }
    }
  });
}


//// Create keyboard  ////

const keyStart = 48 // c3
const keyEnd = 72 // c5
const blackRemainders = [1, 3, 6, 8, 10]

function initializeKeyboard () {
  const keyboard = document.querySelector('.keyboard');
  for (let i = keyStart; i < keyEnd; i++) {
    const whiteKey = document.createElement('div');
    whiteKey.classList.add('key');
    whiteKey.classList.add('key-white');
    whiteKey.setAttribute('data-note', i);
    if (i + 1 < keyEnd && blackRemainders.includes((i + 1) % 12)) {
      const blackKey = document.createElement('div');
      blackKey.classList.add('key');
      blackKey.classList.add('key-black');
      blackKey.setAttribute('data-note', i + 1);
      whiteKey.appendChild(blackKey);
      i++;
    }
    keyboard.appendChild(whiteKey);
  }

  Array.from(document.querySelectorAll('.key')).forEach((key) => {
    // TODO: touch handling way looks cleaner (eg. hit testing by ourselves.)
    //       but there's some fundamental difference the way event dispatched.
    //       so, we might migrate it later.
    key.addEventListener('mousedown', onPress);
    key.addEventListener('mouseup', onRelease);

    key.addEventListener('mouseover', onPress);
    key.addEventListener('mouseout', onRelease);
  });

  keyboard.addEventListener('touchstart', onTouchstart);
  keyboard.addEventListener('touchend', onTouchend);
  keyboard.addEventListener('touchmove', onTouchmove);
}

function initializeStore () {
  const save = document.querySelector("#save-button");
  save.addEventListener('click', () => {
    const http2osc_address = document.querySelector('#http2osc_address').value;
    const osc2midi_address = document.querySelector('#osc2midi_address').value;
    selected = { http2osc_address, osc2midi_address };
    history.push({ http2osc_address, osc2midi_address });
    saveStorage();
  })
}

function initialize () {
  initializeSettings();
  initializeKeyboard();
  initializeStore();
}

initialize();
