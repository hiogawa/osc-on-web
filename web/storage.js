const VERSION = '0.0';
let history = [];
let selected = {
  http2osc_address: "http://localhost:5000",
  osc2midi_address: "osc.udp://localhost:57120",
};

function restoreStorage () {
  let version = window.localStorage.getItem('VERSION');
  if (version && version == VERSION) {
    const historyString = window.localStorage.getItem('HISTORY');
    const selectedString = window.localStorage.getItem('SELECTED');
    if (historyString) {
      history = JSON.parse(historyString);
    }
    if (selectedString) {
      selected = JSON.parse(selectedString);
    }
  }
}

function saveStorage () {
  window.localStorage.setItem('VERSION', VERSION);
  window.localStorage.setItem('HISTORY', JSON.stringify(history));
  window.localStorage.setItem('SELECTED', JSON.stringify(selected));
}

restoreStorage();
